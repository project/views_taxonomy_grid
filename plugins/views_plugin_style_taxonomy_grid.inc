<?php

/**
 * @file
 * Style plugin to render each item in a grid cell.
 */

/**
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_taxonomy_grid extends views_plugin_style_grid {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['columns'] = array('default' => '4');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    $form['columns'] = array(
      '#type' => 'select',
      '#title' => t('Number of columns'),
      '#default_value' => $this->options['columns'],
      '#options' => array(
        '1' => 1, 
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
      ),
    );
  }
  
  /**
   * Add in the 2 taxonomy terms from the filter.  We need these later on for placing the content in the correct cells.
   */
  function query() {
    parent::query();
    $this->view->query->add_field('term_node', 'tid');
    $this->view->query->add_field('term_node2', 'tid');
  }
}

