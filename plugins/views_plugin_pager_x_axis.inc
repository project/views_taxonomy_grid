<?php
/**
 * @file
 * X-axis pager plugin for moving along the x-axis of the grid.
 */

/**
 * Handle paging for taxonomy grid views.  The pager allows the user to shift right or left along the x-axis.
 */
class views_plugin_pager_x_axis extends views_plugin_pager {
  /**
   * This kind of pager does not need to count the number of records.
   */
  function use_count_query() {
    return FALSE;
  }

  /**
   * Because we don't know how many pages there are, we never believe there
   * are more records.
   */
  function has_more_records() {
    return FALSE;
  }

  /*
   * Tell Views what this pager's title is.
   */
  function summary_title() {
    return t('X-axis');
  }

  /**
   * Add a where clause to the query to only get results for the x-axis terms on the current page.
   */
  function query() {
    // First, make sure that there is actually a node available. If not
    // we will bail immediately.
    $this->table_alias = $this->view->query->ensure_table('node');
    if (empty($this->table_alias)) {
      return;
    }

    $page = $_GET['page'] ? $_GET['page'] : 0;

    $x_axis_tids = array_shift($this->view->filter)->value;
    $columns = $this->view->style_plugin->display->display_options['style_options']['columns'];
    $tid_slice = array_slice($x_axis_tids, $page*$columns, $columns);
    
    $this->view->query->add_where(0, "term_node.tid in (%s)", implode(',', $tid_slice));
  }

  function render($input) {
    // This adds all of our template suggestions based upon the view name and display id.
    $pager_theme = views_theme_functions('views_pager_x_axis', $this->view, $this->display);
    // Then, using that, just pass through to the theme.
    return theme($pager_theme, $this, $input);
  }
}
