
This module requires View 3.

Installing this module adds 2 plugins.

The taxonomy grid style plugin creates a grid from the content of your view.  
The only configuration of the style is the number of columns that you'd like 
to display.  You need to add at least 2 'taxonomy: term ID' filters for the style 
to work.  The first 'taxonomy: term' filter is used as the x-axis of the grid 
and its terms as the headers of each column.  The second 'taxonomy: term' filter 
is used as the y-axis of the grid and its terms as the headers of each row.  
The weighting of the vocabulary is used on each axis.

Example taxonomy grid creation:
You must have at least two vocabularies

1. Create a view
2. Add some fields, e.g. node title
3. Add a "taxonomy: term ID" filter and choose a vocabulary and we recommend you use 
the dropdown selection, not the autocomplete.
4. Add a second "taxonomy: term ID" filter in the same way as above.
5. Change the style to Taxonomy Grid and choose the number of columns.

The X-axis pager plugin allows you move along the x-axis of your grid if you 
have more terms in your x-axis vocabulary than columns.  
