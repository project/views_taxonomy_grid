<?php

/**
 * @file views-taxonomy-grid-view.tpl.php
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of columns.
 * - $xterms contains an array of x-axis term objects for the current grid.
 * - $yterms contains an array of all y-axis term objects.
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<table class="views-view-grid cols-<?php print count($rows[0]); ?>">
  <tbody>
    <?php foreach ($rows as $row_number => $columns): ?>
      <tr>
        <?php if ($row_number == 0) : ?>
        <th>&nbsp;</th>
        <?php foreach ($xterms as $xterm): ?>
        <th><?php print $xterm->name; ?></th>
        <?php endforeach; ?>
        <?php endif; ?>
      </tr>
      <tr class="<?php print 'row-'. ($row_number + 1); ?>">
        <th class="row-header"><?php print $yterms[$row_number]->name; ?></th>
        <?php foreach ($columns as $column_number => $items): ?>
          <td class="<?php print 'col-'. ($column_number + 1); ?>">
            <?php foreach ($items as $list_item => $item): ?>
              <?php print $item; ?>
            <?php endforeach; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
