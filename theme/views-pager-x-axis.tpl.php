<?php
/**
 * @file
 * Template file for the x-axis pager.
 *
 * Variables available:
 * - $plugin: The pager plugin object. This contains the view as well as a lot
 *   of other potential data.
 * - $previous: A link to the previous group of x-axis terms.
 * - $current: The current grid.
 * - $next: A link to the next group of x-axis terms.
 */
?>
<div class="views-pager-x-axis">
  <div class="prev-page"><?php print $previous; ?></div>
  <div class="current-page"><?php print $current; ?></div>
  <div class="next-page"><?php print $next; ?></div>
</div>