<?php
/**
 * @file
 * Load files with base classes of the contained classes.
 * This is where all of the logic is kept for determining how to display nodes in a views grid based on taxonomy terms.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_taxonomy_grid_views_plugins() {
  $theme_path = drupal_get_path('module', 'views_taxonomy_grid') . '/theme';
  return array(
    'module' => 'views_taxonomy_grid',
    'style' => array(
      'taxonomy_grid' => array(
        'path' => drupal_get_path('module', 'views_taxonomy_grid') . '/plugins',
        'parent' => 'grid',
        'title' => t('Taxonomy Grid'),
        'theme' => 'views_taxonomy_grid_view',
        'theme path' => $theme_path,
        'help' => t('Creates x and y axis headers based on taxonomy terms and displays content with both tids in each cell.'),
        'handler' => 'views_plugin_style_taxonomy_grid',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal', 
      ),
    ),
    'pager' => array(
      'x_axis' => array(
        'path' => drupal_get_path('module', 'views_taxonomy_grid') . '/plugins',
        'title' => t('X-axis pager'),
        'theme' => 'views_pager_x_axis',
        'theme path' => $theme_path,
        'help' => t('Page by the x-axis term names'),
        'handler' => 'views_plugin_pager_x_axis',
        'uses options' => TRUE,
      ),
    ),
  );
}